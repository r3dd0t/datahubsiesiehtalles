import pandas as pd
from pytrends.request import TrendReq
from influxdb import InfluxDBClient
import datetime
import credentials as cr

pytrend = TrendReq(retries=10)
kw_list =['Corona']
pytrend.build_payload(kw_list)

jetzt = datetime.datetime.now()

bl_list = ['DE-BW', 'DE-BY', 'DE-BE', 'DE-BB', 'DE-HB', 'DE-HH', 'DE-HE', 'DE-MV', 'DE-NI', 'DE-NW', 'DE-RP', 
           'DE-SL', 'DE-SN', 'DE-ST', 'DE-SH', 'DE-TH']

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)

import time
current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

for bl in bl_list:
    df = pytrend.get_historical_interest(kw_list, year_start=int(jetzt.year), month_start=int(jetzt.strftime("%m")), 
                                     day_start=int(jetzt.strftime("%d")), hour_start=(int(jetzt.strftime("%H"))-1), 
                                     year_end=int(jetzt.year), month_end=int(jetzt.strftime("%m")), 
                                     day_end=int(jetzt.strftime("%d")), hour_end=(int(jetzt.strftime("%H"))-1), 
                                     cat=0, geo=bl, gprop='', sleep=60)
    j = {}
    j['measurement'] = 'coronagoogle'
    j['tags'] = {'state': bl}
    j['time'] = curr_time
    j['fields'] = {'value': int(df.iloc[0][0])}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')