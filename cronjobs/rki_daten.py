# RKI Daten
import pandas as pd
from influxdb import InfluxDBClient, DataFrameClient
import time
import credentials as cr

csv_link = 'https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.csv'
df = pd.read_csv(csv_link)

df['Gesamtanzahl_Faelle'] = df['AnzahlFall'].sum()
df['Gesamtanzahl_Todesfaelle'] = df['AnzahlTodesfall'].sum()

# Nach Altersgruppe
grouped_altersgruppe = df.groupby(['Altersgruppe']).sum()
grouped_altersgruppe['Sterbequote'] = grouped_altersgruppe['AnzahlTodesfall'] / grouped_altersgruppe['AnzahlFall'] * 100
grouped_altersgruppe = grouped_altersgruppe.filter(['Altersgruppe', 'AnzahlFall', 'AnzahlTodesfall', 'Sterbequote'])
grouped_altersgruppe.head(20)

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
for row in grouped_altersgruppe.iterrows():
    j = {}
    j['measurement'] = 'rki_corona_nach_altersgruppe'
    j['tags'] = {'altersgruppe': row[0]}
    j['time'] = curr_time
    j['fields'] = {'AnzahlFall': int(row[1][0]), 'AnzahlTodesfall': int(row[1][1]), 'Sterbequote': row[1][2]}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')

# Nach Bundesland
grouped_bl = df.groupby(['Bundesland']).sum()
grouped_bl = grouped_bl.filter(['Bundesland', 'AnzahlFall', 'AnzahlTodesfall'])

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
for row in grouped_bl.iterrows():
    j = {}
    j['measurement'] = 'rki_corona_nach_bundesland'
    j['tags'] = {'bundesland': row[0]}
    j['time'] = curr_time
    j['fields'] = {'AnzahlFall': int(row[1][0]), 'AnzahlTodesfall': int(row[1][1])}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')

# Nach Geschlecht
grouped_sex = df.groupby(['Geschlecht']).sum()
grouped_sex = grouped_sex.filter(['Geschlecht', 'AnzahlFall', 'AnzahlTodesfall'])

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
for row in grouped_sex.iterrows():
    j = {}
    j['measurement'] = 'rki_corona_nach_geschlecht'
    j['tags'] = {'geschlecht': row[0]}
    j['time'] = curr_time
    j['fields'] = {'AnzahlFall': int(row[1][0]), 'AnzahlTodesfall': int(row[1][1])}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')

# Nach Meldedatum
grouped_date = df
grouped_date['Meldedatum'] = pd.to_datetime(grouped_date['Meldedatum'], format='YYYY-mm-ddd', errors='ignore')
grouped_date = grouped_date.groupby(['Meldedatum']).sum()

grouped_date = grouped_date.filter(['Meldedatum', 'AnzahlFall', 'AnzahlTodesfall'])
grouped_date.tail(10)

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
for row in grouped_date.iterrows():
    j = {}
    j['measurement'] = 'rki_corona_nach_datum'
    j['tags'] = {'date': row[0]}
    j['time'] = curr_time
    j['fields'] = {'AnzahlFall': int(row[1][0]), 'AnzahlTodesfall': int(row[1][1])}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')